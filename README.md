# wbtr

The goal of wbtr is to provide easy access to the WhiteboxTools for geospatial data analysis (https://www.uoguelph.ca/~hydrogeo/WhiteboxTools/index.html)

## Installation

You can install the released version of wbtr from [gitlab](https://gitlab.irstea.fr) with:

``` r
library(devtools)
devtools::install_git('https://gitlab.irstea.fr/bjoern.reineking/wbtr.git')
```

You will have to install the WhiteboxTools binaries from https://www.uoguelph.ca/~hydrogeo/WhiteboxTools/download.html and place them in the "ext" folder of the wbtr package.

